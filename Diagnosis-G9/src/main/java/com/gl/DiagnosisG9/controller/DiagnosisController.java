package com.gl.DiagnosisG9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gl.DiagnosisG9.model.Diagnosis;
import com.gl.DiagnosisG9.repository.DiagnosisRepository;

@RestController  // annotation that is itself annotated with @Controller and @ResponseBody
public class DiagnosisController {


	@Autowired // getting object of DiagnosisRepository
	DiagnosisRepository diagRepo;
	
	// mapping to save data 
	@PostMapping("/SaveDiagnosisData")
	public String insertData(@RequestBody Diagnosis diagnosis)
	{
		diagRepo.save(diagnosis);
		return "Diagnosis Data Saved Successfully";
	}
	
	// mapping to get  All Diagnosis Data 
	@GetMapping("/RetrieveData")
	public List<Diagnosis> retrieveData()
	{
		return diagRepo.findAll();
	}
	
	// mapping to get GetdiagnosisById
	@GetMapping("/GetdiagnosisById/{id}")
	public Diagnosis getData(@PathVariable("id") int id)
	{
		Diagnosis diagnosis = diagRepo.getById(id);
		System.out.println(diagnosis.getId());
		return diagnosis;
	}
	
}
