package com.gl.model;

import javax.persistence.Entity;
import javax.persistence.*;

@Entity
@Table(name = "Patient")
public class Patient {

	//Primary key
	@Id
	@Column(name = "Patientid")
	private int patientId;

	//generating getters and setters
	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private String patientName;

	private String email;

	//default constructor
	public Patient() {

	}

	//field constructor
	public Patient(int patientId, String patientName, String email) {
		super();
		this.patientId = patientId;
		this.patientName = patientName;
		this.email = email;
	}
}
