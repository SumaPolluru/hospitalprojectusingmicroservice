package com.gl.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gl.model.Patient;
import com.gl.repository.PatientRepository;

@RestController  // annotation that is itself annotated with @Controller and @ResponseBody
public class PatientController {
	
	@Autowired// getting object of Patient Repositories
	PatientRepository patientRepo;
	
	// mapping to getById
	@GetMapping("/getById/{id}")
	public Patient getPatientId(@PathVariable("id") int id) {
		
		 Patient p = patientRepo.getById(id);
		 return p;
	}
	
	// mapping to savePatientDetails
	@PostMapping("/savePatientDetails")
	public String insert(@RequestBody Patient patient)
	{
		patientRepo.save(patient);
		return "Patient Details Successfully Inserted";
	}


	// mapping to RetrievePatientDetails
	@GetMapping("/RetrievePatientDetails")
	public List<Patient> retrieve()
	{
		List<Patient> patientList = patientRepo.findAll();
		return patientList;
		
	}
}
