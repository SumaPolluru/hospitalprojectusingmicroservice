package com.gl.model;

import javax.persistence.Column;

public class Diagnosis {

	private int id;
	@Column(name = "Diagnosisname")
	private String diagnosisName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDiagnosisName() {
		return diagnosisName;
	}
	public void setDiagnosisName(String diagnosisName) {
		this.diagnosisName = diagnosisName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name = "Description")
	private String description;
	public Diagnosis(int id, String diagnosisName, String description) {
		super();
		this.id = id;
		this.diagnosisName = diagnosisName;
		this.description = description;
	}

	public Diagnosis() {
		
	}
}
