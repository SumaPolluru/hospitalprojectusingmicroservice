package com.gl.model;

public class Patient {

	private int patientId;

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private String patientName;

	private String email;

	public Patient() {

	}

	public Patient(int patientId, String patientName, String email) {
		super();
		this.patientId = patientId;
		this.patientName = patientName;
		this.email = email;
	}
}
