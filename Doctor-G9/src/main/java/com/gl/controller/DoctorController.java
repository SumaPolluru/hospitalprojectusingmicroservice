package com.gl.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.gl.model.Diagnosis;
import com.gl.model.Doctor;
import com.gl.model.Info;
import com.gl.model.Patient;
import com.gl.repository.DoctorRepository;
@RestController  // annotation that is itself annotated with @Controller and @ResponseBody
public class DoctorController {

	@Autowired  // getting object of Doctor Repositories
	DoctorRepository doctorRepo;

	@Autowired  // getting object of RestTemplate 
	RestTemplate template;
	org.springframework.http.HttpHeaders headers =new org.springframework.http.HttpHeaders();

	HttpEntity<String> entity = new HttpEntity<String>(headers);;

	// mapping to InsertDoctor
	@PostMapping("/InsertDoctor")
	public String insert(@RequestBody Doctor doctor) {
		doctorRepo.save(doctor);
		return "Successfully Inserted Doctor Data";
	}

	// mapping to RetrieveData
	@GetMapping("/RetrieveData")
	public List<Doctor> retrieve() {
		return doctorRepo.findAll();
	}

	// mapping to DoctorById
	@PostMapping("/DoctorById/{id}")
	public Doctor doctorData(@PathVariable("id") int id) {
		return doctorRepo.getById(id);
	}

	// mapping to DiagnosisById
	@GetMapping("/DiagnosisById/{id}")
	public Diagnosis diagnosisData(@PathVariable("id") int id) {
		return template
				.exchange("http://localhost:8082/GetdiagnosisById/" + id, HttpMethod.GET, entity, Diagnosis.class)
				.getBody();
	}
	
	//Get by PatientId
			@RequestMapping("/getByPatientId")
			public ModelAndView byIdTest(HttpServletRequest request)
			{
				request.setAttribute("obj","PATIENT_ID"); 
				return new ModelAndView("doctorFunctionalForm");
			}

	// mapping to PatientById 
	@RequestMapping("/PatientById")
	public Patient patientData(@RequestParam int patientId) {
		return template.exchange("http://localhost:8081/getById/" + patientId, HttpMethod.GET, entity, Patient.class)
				.getBody();
	}

	
	// mapping to http://localhost:8083/doctor?idPatient=? & idDiagnosis=? & consultation=?
	@GetMapping("/doctor")
	public Info patientDetails(@RequestParam int idPatient, @RequestParam int idDiagnosis, @RequestParam String consultation)
	{
		Info  pdetails = new Info();
		pdetails.setPatient(template.exchange("http://localhost:8081/getById/" + idPatient,
				 HttpMethod.GET,entity, Patient.class).getBody());
		pdetails.setDoctor(doctorRepo.getByconsultation(consultation));
		pdetails.setDiagnosis(template.exchange("http://localhost:8082/GetdiagnosisById/" + idDiagnosis,
				 HttpMethod.GET,entity, Diagnosis.class).getBody());
		return pdetails;
	}

}
